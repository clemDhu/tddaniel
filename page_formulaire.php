<?php
/**
 * Created by PhpStorm.
 * User: c.duhau
 * Date: 06/06/2018
 * Time: 16:16
 */

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Coupe'tiff</title>
</head>
<body style="background-color: #000000;">
<div class="row justify-content-center mt-5 ">
    <div class="col-sm-7 col-md-5 jumbotron" style="background-color: rgba(255, 255, 255, 0.5);">
<form method="post" action="formulaire.php" enctype="multipart/form-data">

    <div class="form-group">
        <h2 style="color:white;">Formulaire de rendez-vous Chez Coupe'tiff</h2>
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1" style="color:white;">Nom</label>
        <input type="text" name="prenom" placeholder="prenom" class="form-control"/>

    </div>



    <div class="form-group">
        <label for="exampleFormControlTextarea1" style="color:white;">Sexe</label>
        <select class="form-control" name="choixSexe"  >
            <option value="homme">homme</option>
            <option value="Femme">Femme</option>
            <option value="Enfant">Enfant</option>
        </select>
    </div>





      <div class="form-group">
        <label for="exampleFormControlSelect1" style="color:white;">Choisir votre coupe parmis les choix ci-dessous</label>
        <select class="form-control" id="exampleFormControlSelect1" name="typeCoupe">
          <option value="Chignon">Chignon</option>
          <option value="Tresse">Tresse</option>
          <option value="Brushing">Brushing</option>
            <option value="Coupe">Coupe</option>
            <option value="Coupe">Coupe enfant</option>
        </select>
      </div>






    <div class="form-group">
        <label for="exampleFormControlTextarea1" style="color:white;">Autre choix ? Veuillez preciser en dessous</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" name="message" rows="8" cols="45"></textarea>
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1" style="color:white;">Choisir l'heure de rendez-vous</label>
        <select class="form-group" name="choixHeure">

            <option value="8h30">8h30</option>
            <option value="9h">9h</option>
            <option value="9h30">9h30</option>
            <option value="10h">10h</option>
            <option value="10h30">10h30</option>
            <option value="11h">11h</option>
            <option value="11h30">11h30</option>
            <option value="14h30">14h30</option>
            <option value="15h30">15h30</option>
            <option value="16h30">16h30</option>
            <option value="17h30">17h30</option>
        </select>
    </div>

    <input class="form-control" type="submit" value="Reserver" />
</form>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>