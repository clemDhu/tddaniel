<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=td_coiffeur;charset=utf8', 'root', 'root');
$req = $bdd->prepare('INSERT INTO utilisateur (nom,prenom,mail,telephone,mdp) VALUES(:nom,:prenom,:mail,:telephone,:mdp)');
$req->execute(array(
    ':nom' => $_POST['nom'],
    ':prenom' => $_POST['prenom'],
    ':mail' => $_POST['mail'],
    ':telephone' => $_POST['telephone'],
    ':mdp' => $_POST['mdp'],
));

header('Location: http://localhost/tddaniel/page_connexion.php');

?>