<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Coupe'tiff</title>
</head>
<body style="background-color: #000000;">
<div class="row justify-content-center mt-5 ">
    <div class="col-sm-7 col-md-5 jumbotron" style="background-color: rgba(255, 255, 255, 0.5);">
        <h1 style="text-align: center;">Connexion</h1>
<form class="form-signin" action="connexion.php" method="post">
  <div class="form-group">
    <label for="InputEmail">Adresse email</label>
    <input type="email" name="mail" class="form-control" id="InputEmail" required autofocus>
  </div>
  <div class="form-group">
    <label for="InputPassword">Mot de passe</label>
    <input type="password" name="mdp" class="form-control" id="InputPassword"  required>
  </div><br>

    <div class="row">
        <div class="col">
            <button class="btn btn-primary btn-block" type="submit" id="btnConnexion">Connexion</button>
        </div>
        <div class="col">
            <a href="page_inscription.php" class="btn btn-info btn-block">Inscription</a>
        </div>
    </div>
    </div>
    </div>
</form>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>