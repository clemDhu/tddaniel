<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=td_coiffeur;charset=utf8', 'root', 'root');
$req = $bdd->prepare('SELECT id FROM utilisateur WHERE mail = :mail AND mdp = :mdp ');
$req->execute(array(
    ':mail' => $_POST['mail'],
    ':mdp' => $_POST['mdp'],
));
$id = $req->fetch()['id'];
$_SESSION['id'] = $id;

header('Location: http://localhost/tddaniel/page_formulaire.php');
?>

