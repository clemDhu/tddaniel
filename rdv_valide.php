<?php
session_start();
?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Coupe'tiff</title>
</head>
<body style="background-color: #000000;">
<div class="row justify-content-center mt-5 ">
    <div class="col-sm-7 col-md-5 jumbotron" style="background-color: rgba(255, 255, 255, 0.5);">

<?php

echo '<p style="color: white; text-align: center;">Votre rendez vous au nom de '  .$_SESSION['prenom']. 'a bien été ajouté !</p>';


echo '<p style="color: white; text-align: center;">Rappel du rendez-vous : '.$_SESSION['typeCoupe'].' à '.$_SESSION['choixHeure'].'</p>';
        ?>

<p style="color: white; text-align: center;"> Pour prendre une autre rendez-vous cliquez <a href="http://localhost/tddaniel/page_formulaire.php"> ici</a> </p>
</div>
</div>
</body>
</html>