<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=td_coiffeur;charset=utf8', 'root', 'root');
$req = $bdd->prepare('INSERT INTO rdv(prenom,choixSexe,typeCoupe,message,choixHeure) VALUES(:prenom,:choixSexe,:typeCoupe,:message,:choixHeure)');
$req->execute(array(
    'prenom' => $_POST['prenom'],
    'choixSexe' => $_POST['choixSexe'],
    'typeCoupe' => $_POST['typeCoupe'],
    'message' => $_POST['message'],
    'choixHeure' => $_POST['choixHeure'],
));

$_SESSION['prenom'] = $_POST['prenom'];
$_SESSION['choixHeure'] = $_POST['choixHeure'];
$_SESSION['typeCoupe'] = $_POST['typeCoupe'];

header('Location: http://localhost/tddaniel/rdv_valide.php');
?>